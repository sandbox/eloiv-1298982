-- SUMMARY --

Botafoc.cat is a project to be focused on making a base distribution of drupal. The idea is compile the typical modules and configurations that is in vast majority sites using drupal.


-- INSTALLATION --
The next instructions explain how to compile (Download core and modules) a botafoc.cat base distribution. There is a compiled version on http://botafoc.cat ready to download.

- Download latest version via git (https://www.drupal.org/project/1298982/git-instructions)
- Execute "./botafoc.sh". This script generate folder with botafoc.cat compilated.
- Follow the instruccions on the install.
