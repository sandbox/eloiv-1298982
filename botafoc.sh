#!/bin/bash

# Variables
# drupal_version=$(drush status drupal_version --pipe)
STEP=0
BASE_PATH='.'
COMPILED_PATH="botafoc_compiled"

# Arguments
while [[ $# > 1 ]]
do
key="$1"

case $key in
    -bp|--basepath)
    BASE_PATH="$2"
    shift # past argument
    ;;
    -cp|--compiledpath)
    COMPILED_PATH="$2"
    shift # past argument
    ;;
    --default)
    BASE_PATH='.'
    COMPILED_PATH="botafoc_compiled"
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done
STEP=$((STEP + 1))
tput setaf 40; echo "[STEP $STEP] start install botafoc.cat on $COMPILED_PATH folder"; tput sgr0

# Inicialize
drush make --prepare-install --md5 $BASE_PATH/botafoc.cat/makes/botafoc.make $COMPILED_PATH

cp -R $BASE_PATH/botafoc.cat $COMPILED_PATH/profiles

# Create modules subfolders
STEP=$((STEP + 1))
tput setaf 40; echo "[STEP $STEP] Create modules subfolders"; tput sgr0

echo "This folder contain drupal contrib modules. See more on https://www.drupal.org/project/project_module" > $COMPILED_PATH/modules/contrib/README.txt
mkdir $COMPILED_PATH/modules/custom
echo "This folder contain only a drupal custom modules. One custom module is a particular programed modules of the project. And isn't contributed on drupal.org project." > $COMPILED_PATH/modules/custom/README.txt
mkdir $COMPILED_PATH/modules/sandbox
echo "This folder contain drupal sandbox modules. See more on https://www.drupal.org/project/project_module?f[0]=&f[1]=&f[2]=&f[3]=&f[4]=sm_field_project_type%3Asandbox&text=&solrsort=iss_project_release_usage+desc&op=Search" > $COMPILED_PATH/modules/sandbox/README.txt
mkdir $COMPILED_PATH/modules/features
echo "This folder contain drupal custom features modules. See more on https://www.drupal.org/project/features" > $COMPILED_PATH/modules/features/README.txt

# Create themes subfolders
STEP=$((STEP + 1))
tput setaf 40; echo "[STEP $STEP] Create themes subfolders"; tput sgr0

mkdir $COMPILED_PATH/themes/contrib
echo "This folder contain drupal contrib themes. See more on https://www.drupal.org/project/project_theme" > $COMPILED_PATH/themes/contrib/README.txt
mkdir $COMPILED_PATH/themes/custom
echo "This folder contain only a drupal custom themes. One custom theme is a particular programed themes of the project. And isn't contributed on drupal.org project." > $COMPILED_PATH/themes/custom/README.txt


# Copy and rename settings.php
STEP=$((STEP + 1))
tput setaf 40; echo [STEP $STEP] Copy and rename settings.php; tput sgr0
cp $COMPILED_PATH/sites/default/default.settings.php $COMPILED_PATH/sites/default/settings.php
# Define private file on settings.php
mkdir $COMPILED_PATH/sites/default/files
mkdir $COMPILED_PATH/sites/default/private
# chown www-data:www-data $COMPILED_PATH/sites/default/files
# chown www-data:www-data $COMPILED_PATH/sites/default/private
chmod 770 $COMPILED_PATH/sites/default/files
# chmod 760 $COMPILED_PATH/sites/default/private

# echo "\$settings['file_private_path'] = 'sites/default/private';" >> $COMPILED_PATH/sites/default/settings.php
